const allDirs = ["N", "S", "E", "W"];
 
 
 function loadFromUrl(handle, d, id, f) {
    $.ajax({url: handle, data : d, success: function(result){
        const content = f(JSON.parse(result));
        //console.log(content)
        $(id).html(content);
    }});
}

function showBet(k ,b) {
    // show only current bets

    return `<li>For time ${b.validFor}: User # ${b.user} bets on POS ${b.pos} being ${b.expectedVal}</li>`;

}

function reloadAll() {
    // Bets
    loadFromUrl("/bets",  {}, "#log-bets", 
        function (rslt) {
            if (rslt == "") { return "<p>No bets at the moment</p>"; }
            console.log(rslt);
            const mappedVals = rslt.map( x => showBet(x["Key"], x["Record"]) );
            return "<ul>" + mappedVals.join(' ') + "</ul>"; }
    );

    // Balances
    var balanceFn = function(j) { 
        loadFromUrl(
            "/balance", { idx : j},  "#balance" + j, 
            function (rslt) { return `<p>User ${j}: ${rslt}</p>`; }
        );
    };
    for (var i = 1; i <= 3; i++) {
        balanceFn(i);
    }
    
    // Tiles
    $.ajax({url: "/tiles", success: function(result){
        const data = JSON.parse(result)["map"];
        console.log(result)
        for (i in allDirs) {
            const x = allDirs[i];
            val = data[x];
            console.log(x + " " + data[x]);
            if (val) {
                $(".box"+"."+x).addClass("gd");
            } else {
                $(".box"+"."+x).removeClass("gd");
            }
        }
    }});
}


var intervalID = setInterval( reloadAll , 3500);
$(document).ready( reloadAll );