from flask import render_template, jsonify, request, redirect, url_for
from app import app #, root_path, instance_path
from subprocess import check_output
from os import path, chdir, getcwd
from random import randint

DIST_FOLDER = '../typescript/dist/'

# Run a command and return output
def run_node_cmd(cmd_name, rst = []):
    # Name of executable
    basic_cmd = path.join(app.root_path, DIST_FOLDER, "client")

    # Build program and arguments
    cmd_list = [
        "node", basic_cmd,
        "--cmd", cmd_name
    ] + rst

    # Some debugging hints
    cmd_string_dbg = ' '.join(cmd_list)
    print(f"Running: {cmd_string_dbg}")

    #print(f"CWD: {getcwd()}")
    #oldcwd = getcwd()
    #chdir('typescript') # change current directory for wallet

    # Run command
    rslt =  check_output(cmd_list)

    #chdir(oldcwd)

    return rslt

@app.route('/')
@app.route('/index')
def index():
    USER_DEFAULT = "1"
    user = request.args.get("user", USER_DEFAULT)
    success_bet_str = request.args.get("success_bet_str", "")
    return render_template(
        'index.html', 
        user = user,
        success_bet_str = success_bet_str)

@app.route('/admin')
def admin():
    USER_DEFAULT = "1"
    return render_template('admin.html')

@app.route('/bets')
def bets():
    return run_node_cmd('queryAllBets')

@app.route('/balance')
def balance():
    print("idx is ", request.args.get('idx'))
    return run_node_cmd('queryBalance', ["--idx", request.args.get('idx')])

@app.route('/tiles')
def tiles():
    return run_node_cmd('queryTiles')

@app.route('/updatetiles', methods = ['POST'])
def updatetiles():
    output = run_node_cmd('updateTiles')
    return redirect(url_for('admin'))

@app.route('/paybets', methods = ['POST'])
def paybets():
    output = run_node_cmd('payBets')
    return redirect(url_for('admin'))

@app.route('/addbet', methods = ['POST'])
def addbet():
    user = request.form.get('user')
    posLetter = request.form.get('pos')
    #posMap = ['N', 'S', 'E', 'W']
    #pos = str(posMap.index(posLetter)) # We assume it's good
    rst = [
        "--pos", posLetter,
        "--user", user
        ]
    output = run_node_cmd('addBet', rst)
    return redirect(url_for('index', user = user, success_bet_str=output))
    #return redirect( url_for('index') )