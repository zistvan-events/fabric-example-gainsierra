/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { FileSystemWallet, Gateway } from 'fabric-network';
import * as path from 'path';

// NB: change next two line appropriately 
import * as chaincode  from '../../../chaincode/gainsierra/typescript/dist';

const contractName = 'gainsierra';


const ccpPath = path.resolve(__dirname, '..', '..', '..', 'first-network', 'connection-org1.json');

function produceRandomTiles():chaincode.Tiles
{
    let map:Map<chaincode.Pos, boolean> = new Map();
    for (const p of Object.keys(chaincode.Pos)) {
        //console.log(p);
        if (isNaN(Number(p))) {
            map[p] = Math.random()>0.5;
        }
    }
    return { map : map};
}

// TODO: use absolute path in some way for the script
// One solution would be to move wallet to an upper level
async function main() {
    try {

        const args = require('minimist')(process.argv.slice(2));
        //console.log(args);

        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), '../wallet');
        const wallet = new FileSystemWallet(walletPath);
        //console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists('user1');
        if (!userExists) {
            console.error('An identity for the user "user1" does not exist in the wallet');
            console.error('Run the registerUser.ts application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccpPath, { wallet, identity: 'user1', discovery: { enabled: true, asLocalhost: true } });

        // Get the network (channel) our contract is deployed to.
        const network = await gateway.getNetwork('mychannel');

        // Get the contract from the network.
        const contract = network.getContract(contractName);

        // Evaluate the specified transaction from the command line
        const result = await dispatchCmd(args, contract);

        // queryCar transaction - requires 1 argument, ex: ('queryCar', 'CAR4')
        // queryAllCars transaction - requires no arguments, ex: ('queryAllCars')
        //const result = await contract.evaluateTransaction('queryBet', '0');
        //const result = await contract.evaluateTransaction('queryAllBets');

        //console.log(`Transaction has been evaluated, result is: ${result}`);
        const parsedResult = JSON.parse(result);
        console.log(JSON.stringify(parsedResult));
        // Disconnect from the gateway.
        await gateway.disconnect();

    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        process.exit(1);
    }
}

async function dispatchCmd(args, contract):Promise<string>
{
    /* NB: one needs submitTransaction for updates and evaluateTransaction for queries */
    switch (args["cmd"]) {
        case "reset":{  
            await contract.submitTransaction('clearAllState');
            return JSON.stringify("Success."); }
        case "querySimple":{  // requires --key
            const result = await contract.evaluateTransaction('querySimple', args["key"].toString());
            return result; }
        case "queryBet": { // requires --idx
            const result = await contract.evaluateTransaction('queryBet', args["idx"].toString());
            return result; }
        case "queryAllBets": {
            const result = await contract.evaluateTransaction('queryAllBets');
            return result; }
        case "queryBalance": { // requires --idx
            const result = await contract.evaluateTransaction('queryBalance', args["idx"].toString());
            return result; }
        case "payBets": {
            await contract.submitTransaction('payAllBets');
            return JSON.stringify("Success."); }
        case "clearAllBets": { 
            await contract.submitTransaction('clearAllBets');
            return JSON.stringify("Success."); }
        case "queryTiles": {
            const result = await contract.evaluateTransaction('queryTiles');
            return result; }
        case "updateTiles": {
            const randTiles:chaincode.Tiles = produceRandomTiles();
            const randTilesStr = JSON.stringify(randTiles);
            //console.log(randTilesStr);
            await contract.submitTransaction('updateTiles', randTilesStr);
            return JSON.stringify(`Successfully added Tiles!`); }
        case "__increaseBalance": { // require --user
            await contract.submitTransaction('updateBalance', args["user"].toString(), "1");
            return JSON.stringify("Success."); }
        case "addBet": { // requires --idx, --pos and --user
            const basic_bet = 
                { "docType" : "bet", "expectedVal" : true, "pos" : args["pos"],
                    "user" : args["user"], "validFor" : -1 };
            const basic_bet_str = JSON.stringify(basic_bet as chaincode.Bet);
            await contract.submitTransaction(
                'addBet', 
                args["user"].toString(),
                basic_bet_str );
            //console.log(`Successfully submitted bet ${basic_bet_str}`);

            return JSON.stringify(`Successfully added bet for User ${args["user"]}!`); }
        default:
            throw new Error(`Bad command-line argument for --cmd: ${args["cmd"]}`); 
    }
    
} 

main();
