/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { GainSierra } from './gainsierra';
export { GainSierra } from './gainsierra';
export const contracts: any[] = [ GainSierra ];

export {Bet, Tiles, Pos} from './datatypes';
