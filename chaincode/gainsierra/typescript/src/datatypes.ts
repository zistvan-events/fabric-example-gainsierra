export enum Pos {
    N,
    S,
    E,
    W
}

export interface Tiles {
    docType?: string;
    map: Map<Pos, boolean>
}

export interface Bet {
    docType?: string;
    validFor: number;

    pos: Pos;
    expectedVal: boolean;
    //amount: number, // we implicitly assume the bet amount is always a single coin
    user: string
}
