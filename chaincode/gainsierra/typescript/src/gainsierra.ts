/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Context } from 'fabric-contract-api';
import {BasicContract } from './basiccontract';
import { Bet, Tiles } from './datatypes';

function isWinning(bet: Bet, tiles: Tiles)
{
    const actualVal = tiles.map[bet.pos];
    return actualVal == bet.expectedVal;
} 

const APPTIME = "_app_time"

export class GainSierra extends BasicContract {

    public async initLedger(ctx: Context) {
        console.info('============= START : Initialize Ledger ===========');
        await this.reset(ctx);
        console.info('============= END : Initialize Ledger ===========');
    }

    public async reset(ctx:Context) {
        // starting tiles
        const tilesStr = JSON.stringify(
            {"map":{"N":false,"S":false,"E":false,"W":false}} );
        await this.updateTiles(ctx, tilesStr);
    
        const maxUsers = 3; // change as needed

        // update accounts
        for (var i = 1; i <= maxUsers; i++) {
            await this.addSimple(ctx, BasicContract.makeKey("balance", i.toString()), "0");
        }

        //update time to 0
        await this.resetTime(ctx)
    }

    public async clearBalance(ctx:Context) {

        const maxUsers = 3; // change as needed

        // update accounts
        for (var i = 1; i <= maxUsers; i++) {
            await this.addSimple(ctx, BasicContract.makeKey("balance", i.toString()), "0");
        }
    }

    public async querySimple(ctx: Context, k: string): Promise<string>
    {
        const valAsBytes = await ctx.stub.getState(k);  
        if (!valAsBytes || valAsBytes.length === 0) {
            throw new Error(`${k} does not exist`);
        }
        console.log(valAsBytes.toString());
        return JSON.parse(valAsBytes.toString());
    }

    public async addSimple(ctx: Context, k: string, v:string)
    {
        await ctx.stub.putState(k, Buffer.from(JSON.stringify(v)));
    }

    public async updateTiles(ctx:Context, tiles: string) {
        await this.replace(ctx, 'tiles', '0', tiles);
    }

    public async queryTiles(ctx:Context): Promise<string> {
        return await this.query(ctx, 'tiles', '0');
    }

    public async queryAllBets(ctx: Context):Promise<string> {
        return await this.queryFullRange(ctx, 'bet');
    }

    public async queryBet(ctx: Context, id: string):Promise<Bet> {
        return await this.query(ctx, 'bet', id);
    }

    public async payAllBets(ctx:Context) {
        /* 
          Logic of this simple payment method:
          - Gather all the money from bets (call this resulting value V)
          - See how many bets are won (call this number m)
          - give each of the m winners V/m.
          NB: for this to be profitable of course the chance of everybody
           winning should be sort of low.
        */

        const cur_time = await this.getTime(ctx);

        const betsKV_str = await this.queryAllBets(ctx);
        console.log(betsKV_str);
        const betsKV = JSON.parse(betsKV_str);
        const betsKeys = betsKV.map(item => item["Key"]); 
        const all_bets = betsKV.map(item => item["Record"]); 
        const bets = all_bets.filter(b => b.validFor == cur_time );
        console.log("valid bets:");
        console.log(bets);

        // V is the total value of all bets
        const V = bets.length;

        // disable all bets 
        await this.clearAllBets(ctx);

        const tilesStr = await this.queryTiles(ctx);
        const tiles:Tiles = JSON.parse(tilesStr);
        console.log("tiles:");
        console.log(tiles);
        
        // find winners
        const winningBets = bets.filter(b => isWinning(b, tiles) );
        console.log("winningBets:");
        console.log(winningBets);
        const m = winningBets.length;

        const win = V/m;
        console.log("Win:");
        console.log(win);

        for (let w of winningBets) {
            await this.updateBalance(ctx, w.user, win);
        }

        await this.incrementTime(ctx);
    }

    public async queryBalance(ctx: Context, user: string) {
        return await this.query(ctx, 'balance', user);
    }

    public async updateBalance(ctx: Context, user: string, delta: number) {
        const curBalance = await this.query(ctx, 'balance', user);
        console.log(`curBalance is ${curBalance}`);
        const newBalance = Number(curBalance) + Number(delta);
        console.log(`newBalance is ${newBalance}`);
        await this.replace(ctx, 'balance', user, newBalance);
    }

    public async addBet(ctx:Context, id: string, bet: string) { // bet: Bet
        const betObj = JSON.parse(bet);
        betObj.validFor = await this.getTime(ctx);
        await this.create(ctx, 'bet', id, betObj);

    }

    public async clearAllBets(ctx: Context) {
        const betsKV_str = await this.queryAllBets(ctx);
        const betsKV = JSON.parse(betsKV_str);
        console.log(`Output of bets KV: ${betsKV}`); 
        const betsKeys = betsKV.map(item => item["Key"]); 

        for (const k of betsKeys) {
            const valBytes:string = await this.queryByFullKey(ctx, k);
            console.log("queryByFullKey done");
            console.log(valBytes);
            const val = valBytes;
            console.log("val:");
            console.log(val);
            val["validNow"] = false;
            await ctx.stub.putState(k, Buffer.from(JSON.stringify(val))); 
        }
    }

    async resetTime(ctx: Context) {
        await ctx.stub.putState(APPTIME, Buffer.from(JSON.stringify(1)));
    }

    public async getTime(ctx: Context) : Promise<number> {
        const valAsBytes = await ctx.stub.getState(APPTIME);
        return JSON.parse(valAsBytes.toString());
    }

    async incrementTime(ctx: Context) {
        const curTime = await this.getTime(ctx);
        const nextTime = curTime+1;
        await ctx.stub.putState(APPTIME, Buffer.from(JSON.stringify(nextTime)))
    }    
}
