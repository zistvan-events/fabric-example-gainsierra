# 0. Warm up: Display current application time

GainSierra works in "time steps". At the beginning of each period users can bet on a tile and at the end of the period the tiles are updated and then payouts are made, [moving to the next time step](https://gitlab.software.imdea.org/zistvan-events/fabric-example-gainsierra/blob/exercise1/chaincode/gainsierra/typescript/src/gainsierra.ts#L129).

Our client website, however, does not show any information related to time, which can be confusing. Let's add the current application time to the website.

To do this, we will first add a new action to our `gainsierra/typescript/src/client.ts` on the GainSierra server side. We can use the `getTime` operation that the chaincode already has to extract the current time. To be able to call this action from the browser, we'll add a route to `gainsierra/app/routes.py`, for instance, as `/apptime`.

Restart the web server and try accessing `127.0.0.1:5000/apptime` . Observe what is output!

Now that we have a way of querying the application time, we need to insert this into our website. We'll edit `gainsierra/app/templates/base.html ` to include an anchor element in the body with, for instance, the id of 'app-time' : `<h2>Tiles at <a id="app-time"></a></h2>`

Observe that in `gainsierra/app/static/client.js` there is already a way of populating a specific element in the HTML from a URL. Use the same mechanism to load the time into the element you added above.

# 1. Display only current bets

At the moment, the application uses the UserID to create the keys of bets in the ledger ("bet"+UserID). This means that each user can have only one bet (newer bets overwrite older ones) but in the current website bets from the previous timestep will be still visible. Let's fix this!

Hint 1: This task can be performed from the client side, without changing the chaincode.

Hint 2: There is already a way of querying the application time, and a function called [showBet](https://gitlab.software.imdea.org/zistvan-events/fabric-example-gainsierra/blob/b82e73780fb0fd54b305bd9eda9586ab80e2f1fe/gainsierra/app/static/client.js#L12) that could be used to conditionally show the bet if its `validFor` matches the current time.

# 2. Keep a history of all bets

The way the ID of the bet is generated, there can be at most 1 bet per user. It would be, however, more meaningful to keep track of all bets that have been made on the platform. Since we cannot entrust the user application to create IDs for the bets, we need to implement this functionality as part of the chaincode.

Hint 1: You should modify the function that insert bets in the chaincode side. Don't use randomness for the ID!

Hint 2: Make sure that the chosen ID scheme doesn't brake the query all bets call! Adjust as necessary.

## Bonus task

Since we are already making a distinction between currently valid bets (their time corresponds to the current time) and all bets, the chaincode should offer a way of 1) asking for a list of all bets and 2) a list of currently valid bets. This would enable us to remove the "filtering" code from the client side and reduce data communications. Can you make this work?

# 3. Make tile updating bet-dependent

At the moment, updating the tiles before paying out the current round involves picking their state at random. This is, of course, too simplistic. In this task you will have to implement the following logic for deciding the next state of each tile:

* if more than one person bets on a tile, it will be green (saved)
* if less than one person bets on a tile, it will be yellow (wasted)
* if no one bets on a tile, it stays as it is

This functionality should be implemented in the chaincode by modifying the part that updates the tiles.

Hint 1: The client sends right now a JSON string to the chaincode describing the next state of the tiles -- this will have to be ignored. Ideally, you should remove the random generation, as well as the argument, from the code.

Hint 2: You can re-use the code for getting valid bets in the payout section to have at hand all the ones you need to count. A Map<Pos, number> could be used to count for each tile.

## Bonus task

 It is awkward that once tile update becomes a chaincode-based operation, the administrator still has to manually perform it before payout. For this reason, we should merge the update computation with the payout and replace the two buttons with a single one.

